function defineReactive (data, key, val) {

  if (arguments.length === 2) {
    val = data[key]
  }

  const dep = new Dep()

  let childOb = observe(val)
  Object.defineProperty(data, key, {
    enumerable: true,
    configurable: true,

    get () {
      if (Dep.target) {
        dep.depend()
        if (childOb) {
          // 这里对于对象来说，是多收集了一次,
          // 主要作用是针对数组的
          // 这里的childOb即observer对象，在上面observe过程中，已经被注册到数组中的__ob__中,，所以在这里childOb.dep.depend()后，通过数组 .__ob__同样可以拿到这个对象,
          childOb.dep.depend()
          // console.log(childOb)

        }
      }
      return val
    },

    set (newValue) {

      if (newValue === val) {
        return
      }
      val = newValue
      dep.notify()
      childOb = observe(newValue)
    }
  })
}

function observe (value) {

  if (typeof value !== 'object') {
    return
  }

  var ob;

  if (typeof value.__ob__ !== 'undefined') {
    ob = value.__ob__
  } else {

    ob = new Observer(value)
  }
  return ob
}



function def (obj, key, value, enumerable) {
  // console.log('hahha')
  Object.defineProperty(obj, key, {
    value,
    enumerable: !!enumerable,
    writable: true,
    configurable: true
  })
}

class Dep {
  constructor() {
    this.subs = []
  }
  // addSub (sub) {
  //   this.subs.push(sub)
  // }
  depend () {
    if (Dep.target) {
      this.subs.push(Dep.target)
    }
  }
  notify () {
    for (let i = 0, l = this.subs.length; i < l; i++) {
      this.subs[i].update()
    }
  }
}

function parsePath (expression) {
  var path = expression.split('.')
  return (obj) => {
    if (!obj) {
      return
    }
    for (let i = 0, l = path.length; i < l; i++) {
      obj = obj[path[i]]
    }
    return obj
  }
}

class Watcher {
  constructor(obj, expression, callback) {
    this.target = obj
    this.getfun = parsePath(expression)
    this.callback = callback
    this.value = this.getVal()
  }

  getVal () {
    var value;
    // 获得当前observer对象，
    Dep.target = this

    try {
      // 主动触发get()，在get()也可以拿到Dep.target
      value = this.getfun(this.target)
    }
    finally {
      Dep.target = null
    }
    return value
  }

  update () {
    const newVal = this.getVal()
    if (newVal !== this.value || typeof newVal == 'object') {
      const oldValue = this.value
      this.value = newVal
      this.callback.call(this.target, oldValue, newVal)
    }
  }
}

class Observer {
  constructor(obj) {
    this.dep = new Dep()
    def(obj, '__ob__', this, false)
    if (Array.isArray(obj)) {
      Object.setPrototypeOf(obj, arrayMethods)
      this.observeArray(obj)
    } else {
      this.walk(obj)
    }
  }
  walk (obj) {
    Object.keys(obj).forEach(k => defineReactive(obj, k, obj[k]))
  }
  observeArray (arr) {
    for (let i = 0, l = arr.length; i < l; i++) {
      observe(arr[i])
    }
  }

}

const arrayPrototype = Array.prototype
// ES5新增的Object.create()方法创建一个新对象，使用现有的对象（传入的第一个参数）来作为新创建对象的prototype。(即将创建的新对象的__proto__指向已存在对象的原型)
const arrayMethods = Object.create(arrayPrototype)

const methodsNeedChange = [
  'push',
  'pop',
  'shift',
  'unshift',
  'sort',
  'reverse',
  'splice'
]

methodsNeedChange.forEach(methodName => {
  const originMethod = arrayPrototype[methodName]
  def(arrayMethods, methodName, function () {
    const args = [...arguments]
    let inserted = []
    switch (methodName) {
      case 'push':
      case 'unshift':
        inserted = args
        break

      case 'splice':
        inserted = args.splice[2]
        break
    }
    if (inserted.length) {
      // ob.observeArray(inserted)
    }

    console.log('array changed')
    const ob = this.__ob__
    // 输出数组的watcher
    console.log(ob.dep.subs)
    ob.dep.notify()
    const res = originMethod.apply(this, arguments)
    return res;
  }, false)
})


var obj = {
  a: 1,
  b: {
    c: 10
  },

  g: [23, 34, 56, 3, 1, 52]
}


const res = observe(obj)

new Watcher(obj, 'g', (oldVal, newVal) => {
  console.log('oldValue', oldVal)
  console.log('newValue', newVal)
})
console.log(obj.g.__ob__)


