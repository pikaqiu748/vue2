import Dep from './Dep.js'
import observe from './Observe.js'

// 闭包，在内部函数访问val
function defineReactive (data, key, val) {
  // console.log(data, key)
  if (arguments.length === 2) {
    val = data[key]
  }

  const dep = new Dep()
  // 子元素要进行observe，至此形成了递归。这个递归不是函数自己调用自己，而是多个函数、类循环调用
  // console.log('val', val)
  let childOb = observe(val)

  Object.defineProperty(data, key, {
    enumerable: true,
    configurable: true,

    get () {
      // 如果处于依赖收集阶段
      // 全局属性Dep.target在Watcher中的构造器中,调用get()方法,里面,有Dep.target = this;即将Dep.target赋值为Watcher实例对象,在index.js中,observe后,就会有new Watcher(obj,'a.m.n',()=>{
      // 这是传入的回调函数
      // })

      // 如果自己在查看赋值，虽然会触发get，但是会直接返回，因为此时Dep.taget为空，只有在new Watcher的时候，才会进入if，并执行depend()，即如果
      // 是Watcher的话就addSub()
      // 说明这是watch 引起的，说明有watcher要订阅这个数据
      // Dep.target在new Watcher的Watcher中的get中已经赋值
      // 注意只有在new Watcher的时候才会被赋值
      if (Dep.target) {
        // console.log('this', this)
        // console.log('dep', dep)
        // 用Observe中的dep实例，执行dep.depend()
        dep.depend()
        if (childOb) {
          // console.log('测试')
          childOb.dep.depend()
        }
      }
      return val
    },

    set (newValue) {
      // console.log('修改值')
      // 判断newValue==val,可以防止递归notify,比如在new Watcher的回调函数中，给new Watcher的订阅属性再次赋值，如果不判断，newValue==val,则会首先notify一次，之后在回调函数中，会再次赋值，又会再次notify,会这样一直递归，加了这个判断，即使在回调函数中，改变这个订阅的属性，如果一样，就会直接return,不会notify,否则，也只会notify一次，因为下次判断发现newValue==val,会直接return
      if (newValue === val) {
        return
      }
      val = newValue
      // 当设置了新值，新值也要observe
      childOb = observe(newValue)
      // 发布订阅模式
      dep.notify()
    }

  })
}

export default defineReactive;