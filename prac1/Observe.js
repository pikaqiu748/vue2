import Observer from './Observer.js'

// 创建observe函数，注意函数名字没有
function observe (value) {
  // 即只有对象才可以有__ob__属性,否则直接返回到defineReactive中的obseve处，只定义get,set
  if (typeof value !== 'object') {
    return
  }

  var ob;
  // ob = new Observer(value)

  if (typeof value.__ob__ !== 'undefined') {
    ob = value.__ob__
  } else {
    // 否则，往value上添加__ob__属性
    // __ob__:Observer实例，
    // Observer的构造函数中又有new Dep(),所以又有dep实例，dep实例中又有subs[],
    // 所以，只要是对象，在new Observer后，都会在这个对象中，拥有observer实例，observer实例中又有Dep实例，dep实例有subs[]属性,为对象初始化好这些，在后面new Watcher的时候，会用到
    ob = new Observer(value)
  }
  return ob
}

export default observe;