import def from './utils.js'

const arrayPrototype = Array.prototype
// console.log(arrayPrototype.pop)

// 以Array.prototype 为原型，创建arrayPrototype对象
const arrayMethods = Object.create(arrayPrototype)
// console.log(arrayMethods.pop)
const methodsNeedChange = [
  'push',
  'pop',
  'shift',
  'unshift',
  'splice',
  'sort',
  'reverse'
]

methodsNeedChange.forEach(methodName => {

  // 备份原来的方法
  const original_method = arrayPrototype[methodName]
  // 定义新方法
  def(arrayMethods, methodName, function () {
    // this是调用的数组
    // 再调用原来的功能
    const result = original_method.apply(this, arguments)
    // 把类数组对象变为数组
    const args = [...arguments]
    //把这个数组身上的_ob_取出来，_ob_已经被添加了，为什么已经被添加了?因为数组肯定不是最高层，比如obj.g属性是数组，obj不能是数组，第一次遍历obj这个对象的第一层的时候，已经给g属性(就是这个数组)添加了_ob_属性。
    const ob = this.__ob__
    //有三种方法push\unshift\splice能够插入新项，现在要把插入的新项也要变为observe的
    //有三种方法推送\unShift\拼接能够插入新项，现在要把插入的新项也要变为观察的
    let inserted = []
    switch (methodName) {
      case 'push':
      case 'unshift':
        inserted = args
        break
      case 'splice':
        // splice格式是splice(下标，数量，插入的新项)
        // 取出子数组
        inserted = args.slice(2)
        break
    }
    if (inserted.length) {
      ob.observeArray(inserted)
    }

    console.log('经过改变后的方法')
    ob.dep.notify()
    return result
  }, false)
})

export default arrayMethods;