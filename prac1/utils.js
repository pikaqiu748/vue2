// 分别是{}数据对象，__ob__,Observer对象，false:console.log(数据对象的时候，不会出现__ob__:Observer)
function def (obj, key, value, enumerable) {
  // console.log('def obj:', obj)
  // console.log('def key:', key)
  // console.log('def this:', value)


  Object.defineProperty(obj, key, {
    value,
    enumerable: !!enumerable,
    writable: true,
    configurable: true
  })
}

export default def;