import def from './utils.js'
import defineReactive from './defineReactive.js'
import arrayMethods from './Array.js'
import observe from './Observe.js'
import Dep from './Dep.js'

// Observer类的目的是:将一个正常的object转换为每个层级的属性都是响应式(可以被侦测的）的object
// 将一个object的每一层的属性，都转换为响应性的Observer 类 主要做了以下事情遍历 data 下的每一个属性，若是对象，则 执行 new Observer() ,在对象上新增__ob__属性,该属性的值为 Observer 的实例劫持对象属性的变化，在 getter 的时候，拿到 Observer 实例的dep实例，执行dep.depend()，代码如下:
// const ob = this.__ob__
// ob.dep.depend();


export default class Observer {
  constructor(obj) {
    // 这里的dep是为了数组设置的,在拦截器中可以通过this.__ob__.dep拿到他。
    this.dep = new Dep()
    // 在遍历value的时候，不希望遍历出__ob__，所以要在def中将__ob__的enumerable设置为false
    // this就是实例本身,即让value.__ob__=这次new的实例
    // false是enumerable,false表示不可遍历出这个属性

    // def函数,给object添加__ob__属性,值为Observer实例对象
    def(obj, '__ob__', this, false)
    // console.log('我是Observer构造器', value)
    // 检查是数组还是对象
    if (Array.isArray(obj)) {
      // 如果是数组，则将数组的原型指向arrayMethods，arrayMethods是改写后的方法
      Object.setPrototypeOf(obj, arrayMethods)
      // 让数组observe
      this.observeArray(obj)
    } else {
      // 如果不是数组
      this.walk(obj)
    }
  }

  // 如果不是数组,逐层defineReactive，所以在walk中调用defineReactive方法

  walk (obj) {
    //defineReactive方法中有原生js=> Object.defineProperty来定义属性,例如enumerable get(),set()
    // 但是在Object.defineProperty之前,先调用observe,进行深层次的遍历,
    // 整个循环过程为observe=>new Observer()=>如果是对象=>defineReactive=>又先调用observe,一直递归,
    // observe中会判断是否为对象,即到了不是对象时,return,返回上一层
    Object.keys(obj).forEach(k => defineReactive(obj, k, obj[k]))
  }

  // 数组遍历
  observeArray (arr) {
    for (let i = 0, l = arr.length; i < l; i++) {
      // 逐项observe
      observe(arr[i])
    }
  }
}