// Watcher实现自动添加依赖,watcher是订阅者
import Dep from "./Dep.js"

var uid = 0
export default class Watcher {

  constructor(obj, expression, callback) {
    // console.log('我是Watcher构造器', 'expression为:', expression)
    this.id = uid++
    // target是传入的obj
    this.target = obj
    // parsePath返回一个函数
    this.getter = parsePath(expression)
    this.callback = callback
    // get()中会调用getter函数
    this.value = this.get()
    // console.log('构造函数中的watcher实例', this)
  }

  update () {
    this.getAndInvoke(this.callback)
  }


  // new Watcher()中传过来的callback
  getAndInvoke (callback) {
    const value = this.get()

    if (value !== this.value || typeof value == 'object') {
      const oldValue = this.value
      // 对当前watcher的value进行更新
      // 记录old值，用于下次
      this.value = value
      callback.call(this.target, value)
    }
  }

  get () {
    // 进入依赖收集阶段,让全局的Dep.target设置为Watcher这次的实例本身
    var value;
    // 在此处记住这个实例化的Watcher实例对象,然后在下面的getter里面,getter就是parsePath中返回的箭头函数,
    // 下面的this.getter(obj),就是执行parsePath中的箭头函数,而箭头函数里面有obj[segment[i]],则会触发在defineReavtive中的get函数,别忘了,Dep.target=Watcher实例,是全局属性,所以在get()中也可以访问,并在get()中,通过dep.depend(),将此时的Dep.target,即Watcher实例,放到Dep的subs中,最后，改变数据时候，触发setter,执行ob.dep.notify()

    // 然后在下面将Dep.target=null,记录下一次的Watcher


    // 判断是不是Watcher的构造函数调用，如果是，说明他就是这个属性的订阅者
    // 果断将他addSub()中去，那问题来了，
    // 我怎样判断他是Watcher的this.get调用的，而不是我们普通调用的呢。
    // 对，在Dep定义一个全局唯一的变量，
    Dep.target = this;
    // this.target=obj,即new Watcher传入的数据
    const obj = this.target
    try {
      value = this.getter(obj)
      // console.log('值为', value)

    } finally {
      Dep.target = null
    }
    return value
  }
}


// {
// a: {
//   m: {
//     n: 5
//      }
//   },
// }
function parsePath (str) {
  var segment = str.split('.')
  return (obj) => {
    if (!obj) return;
    // segment=['a','m','n']
    for (let i = 0; i < segment.length; i++) {
      // 触发get()
      obj = obj[segment[i]]
    }
    return obj
  }
}