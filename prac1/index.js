import defineReactive from './defineReactive.js'
import observe from './Observe.js'
import Watcher from './Watcher.js'
var obj = {
  a: {
    m: {
      n: 5
    }
  },
  b: 100,
  c: {
    g: [23, 34, 56, 3, 1, 52]
  }
}



const res = observe(obj)


new Watcher(obj, 'c.g', (val) => {
  console.log('newValue,oldValue', val)
})

// obj.g = [1, 2, 3, 45, 6]
// obj.g = [23]
obj.c.g = [1, 2, 3, 4, 5]

obj.c.g.push(100)