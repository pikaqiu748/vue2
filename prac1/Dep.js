// Dep在Oberver中实例化
// defineReactive也需要引入
var uid = 0
export default class Dep {
  constructor() {
    // console.log('我是Dep构造器')
    this.id = uid++
    // 用数组存储自己的订阅者。subs是英语subscribes订阅者的意思。
    // 存放watcher的实例
    this.subs = []
  }
  // 添加订阅 
  addSub (sub) {
    this.subs.push(sub)
  }
  // Dep.target就是一个我们自己指定的全局的位置，你用window.target也行，只要是全局唯一，没有歧义就行
  depend () {
    if (Dep.target) {
      this.addSub(Dep.target)
    }
  }

  // 通知更新
  notify () {
    // console.log('notify')
    // 浅克隆一份
    const subs = this.subs.slice()
    // 遍历
    for (let i = 0, l = subs.length; i < l; i++) {
      // // 存放的是atcher的实例
      subs[i].update()
      // console.log('watcher', subs[i])
    }
  }
}